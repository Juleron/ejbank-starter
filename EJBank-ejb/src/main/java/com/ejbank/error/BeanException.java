package com.ejbank.error;

import java.util.HashMap;

public class BeanException extends Exception {

	private static final long serialVersionUID = 1L;
	private static final HashMap<Integer, MessageCode> messages;
	
	static {
		messages = new HashMap<>();
		for (MessageCode messageCode: MessageCode.values()) {
			messages.put(messageCode.getCode(), messageCode);
		}
	}
	
	public static BeanException createException(MessageCode messageCode) {
		return new BeanException(messageCode.getCode(), null);
	}
	
	public static BeanException createException(MessageCode messageCode, Exception cause) {
		return new BeanException(messageCode.getCode(), cause);
	}
	
	private final Exception cause;
	private final int code;
	
	private BeanException(int code, Exception cause) {
		if (!messages.keySet().contains(code)) {
			throw new IllegalArgumentException("Cannot create the BeanException because the given code doesn't exist");
		}
		
		this.code = code;
		this.cause = cause;
	}
	
	@Override
	public String getMessage() {
		StringBuilder builder = new StringBuilder();

		// Adds the client message
		builder.append(messages.get(code).getMessage());
		
		// Adds the original exception message
		if (cause != null) {
			builder.append("\nCaused by :\n");
			builder.append(cause.getMessage());
		}
		
		return builder.toString();
	}
	
	public String getClientMessage() {
		return messages.get(code).getMessage();
	}
	
	
}
