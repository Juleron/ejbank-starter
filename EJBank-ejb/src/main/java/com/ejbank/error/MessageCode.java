package com.ejbank.error;

public enum MessageCode {
	UNKNOWN_USER(1, "L'utilisateur recherché n'existe pas"),
	UNKNOWN_ACCOUNT(2, "Le compte recherché n'existe pas"),
	UNKNOWN_TRANSACTION(3, "La transaction recherchée n'existe pas"),
	UNKNOWN_ACCOUNT_TYPE(4, "Le type de compte recherché n'existe pas"),
	DATABASE_ACCESS_ERROR(10, "La base de données est innaccessible"),
	UNKNOWN_RESSOURCE(20, "La ressource demandée n'existe pas"),
	LACK_OF_PATH_PARAMETER(21, "Un argument dans le chemin de la ressource est manquant"),
	LACK_OF_GET_PARAMETER(22, "Un argument de type GET est manquant dans le chemin de la ressource")
	;
	
	private final int code;
	private final String message;
	
	public Integer getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	private MessageCode(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
}
