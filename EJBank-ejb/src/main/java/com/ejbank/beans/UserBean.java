package com.ejbank.beans;

import com.ejbank.entity.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
@LocalBean
public class UserBean implements IUserBean {

    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;

    @Override
    public List<User> getUsers() {
        TypedQuery<User> typedQuery = em.createNamedQuery(User.findMultipleUsers, User.class);
        return typedQuery.getResultList();
    }

    @Override
    public User getUser(Integer userId) {
        return em.find(User.class, userId);
        /*TypedQuery<User> typedQuery = em.createNamedQuery(User.findSingleUser, User.class);
        typedQuery.setParameter("userId", userId);
        return typedQuery.getSingleResult();*/
    }
}
