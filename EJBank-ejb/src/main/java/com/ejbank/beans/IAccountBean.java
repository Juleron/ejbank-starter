package com.ejbank.beans;

import com.ejbank.entity.Account;
import com.ejbank.entity.User;
import com.ejbank.payload.TransactionListPayload;
import com.ejbank.payload.TransactionRequest;
import com.ejbank.payload.TransactionResponse;
import com.ejbank.payload.TransactionResultValid;
import com.ejbank.payload.TransactionValidation;

import javax.ejb.Local;
import java.math.BigDecimal;
import java.util.List;

@Local
public interface IAccountBean {
    List<Account> getAccounts(int userId);

    Account getAccount(int accountId);

    int getTransactionsToAppliedWithUserId(Integer userId);

    int getTransactionsToAppliedWithAccountId(Account account);

    TransactionResponse insertTransaction(TransactionRequest transactionRequest, User user);

    BigDecimal getInterests(Account a);

    TransactionListPayload getTransactions(Integer accountId, Integer offset, Integer userId);

    TransactionResultValid updateTransactionValidation(TransactionValidation transactionValidation);
}
