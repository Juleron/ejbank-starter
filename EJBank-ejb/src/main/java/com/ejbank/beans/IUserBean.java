package com.ejbank.beans;

import com.ejbank.entity.User;

import javax.ejb.Local;
import java.util.List;

@Local
public interface IUserBean {
    List<User> getUsers();

    User getUser(Integer userId);
}
