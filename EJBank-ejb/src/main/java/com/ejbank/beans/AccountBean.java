package com.ejbank.beans;

import com.ejbank.entity.Account;
import com.ejbank.entity.Transaction;
import com.ejbank.entity.User;
import com.ejbank.payload.ITransactionPayload;
import com.ejbank.payload.TransactionFromPayload;
import com.ejbank.payload.TransactionListPayload;
import com.ejbank.payload.TransactionRequest;
import com.ejbank.payload.TransactionResponse;
import com.ejbank.payload.TransactionResultValid;
import com.ejbank.payload.TransactionState;
import com.ejbank.payload.TransactionToPayload;
import com.ejbank.payload.TransactionValidation;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class AccountBean implements IAccountBean {

    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;

    @Resource
    UserTransaction utx;

    @Override
    public List<Account> getAccounts(int userId) {
        User user = em.find(User.class, userId);
        return user.getAccounts();
    }

    @Override
    public Account getAccount(int accountId) {
        return em.find(Account.class, accountId);
    }

    @Override
    public BigDecimal getInterests(Account a) {
        BigDecimal rate = a.getAccountType().getRate();
        return a.getBalance().multiply(rate.divide(new BigDecimal(100)));
    }

    @Override
    public TransactionListPayload getTransactions(Integer accountId, Integer offset, Integer userId) {

        Query query = em.createNamedQuery(Transaction.getTransactions, Transaction.class);
        query.setParameter("accountId", getAccount(accountId));
        query.setFirstResult(offset);
        query.setMaxResults(3);

        TransactionListPayload transactionListPayloads = new TransactionListPayload();
        List<ITransactionPayload> iTransactionPayloads = new ArrayList<>();

        List<Transaction> transactionList = query.getResultList();
        for (Transaction transaction : transactionList) {
            // compte source
            Account from = transaction.getAccountIdFrom();
            // compte destination
            Account to = transaction.getAccountIdTo();

            if (from.getUser().getId().equals(userId)) {
                TransactionFromPayload transactionFromPayload = new TransactionFromPayload();
                transactionFromPayload.setAmount(transaction.getAmount());
                transactionFromPayload.setAuthor(transaction.getAuthor().getFirstname() + " " + transaction.getAuthor().getLastname());
                transactionFromPayload.setComment(transaction.getComment());
                transactionFromPayload.setDate(transaction.getDate());
                transactionFromPayload.setDestination(to.getAccountType().getName());
                transactionFromPayload.setDestination_user(to.getUser().getFirstname());
                transactionFromPayload.setId(transaction.getId());
                transactionFromPayload.setSource(from.getAccountType().getName());
                transactionFromPayload.setState(transaction.getApplied() == 0 ? TransactionState.APPLYED : TransactionState.TO_APPROVE);
                iTransactionPayloads.add(transactionFromPayload);
            } else {
                TransactionToPayload transactionToPayload = new TransactionToPayload();
                transactionToPayload.setAmount(transaction.getAmount());
                transactionToPayload.setAuthor(transaction.getAuthor().getFirstname() + " " + transaction.getAuthor().getLastname());
                transactionToPayload.setDate(transaction.getDate());
                transactionToPayload.setDestination(to.getAccountType().getName());
                transactionToPayload.setSource_user(from.getUser().getFirstname());
                transactionToPayload.setId(transaction.getId());
                transactionToPayload.setSource(from.getAccountType().getName());
                transactionToPayload.setState(transaction.getApplied() == 0 ? TransactionState.APPLYED : TransactionState.TO_APPROVE);
                iTransactionPayloads.add(transactionToPayload);
            }

        }

        Query queryTotal = em.createNamedQuery(Transaction.getNumberTransactions, Transaction.class);
        queryTotal.setParameter("accountId", getAccount(accountId));

        Long total = (Long) queryTotal.getSingleResult();

        transactionListPayloads.setTotal(total.intValue());
        transactionListPayloads.setTransactions(iTransactionPayloads);

        return transactionListPayloads;
    }

    @Override
    public TransactionResultValid updateTransactionValidation(TransactionValidation transactionValidation) {
        TransactionResultValid transactionResultValid = new TransactionResultValid();
        if (!transactionValidation.isApprove()) {
            em.remove(em.find(Transaction.class, transactionValidation.getTransaction()));
            transactionResultValid.setMessage("Transaction annulée.");
            transactionResultValid.setResult(false);
            return transactionResultValid;
        }

        try {
            utx.begin();

            Transaction transaction = em.find(Transaction.class, transactionValidation.getTransaction());
            transaction.setApplied(0);

            Account accountSource = transaction.getAccountIdFrom();
            Account accountDestination = transaction.getAccountIdTo();

            accountSource.setBalance(accountSource.getBalance().subtract(transaction.getAmount()));
            accountDestination.setBalance(accountDestination.getBalance().add(transaction.getAmount()));

            em.flush();

            utx.commit();
        } catch (NotSupportedException | SystemException | HeuristicMixedException | HeuristicRollbackException | RollbackException e) {
            try {
                utx.rollback();
            } catch (SystemException ex) {
                ex.printStackTrace();
            }
        }

        transactionResultValid.setResult(true);
        transactionResultValid.setMessage("Transaction validée.");

        return transactionResultValid;
    }

    @Override
    public int getTransactionsToAppliedWithUserId(Integer userId) {
        List<Account> accountList = getAccounts(userId);
        int counter = 0;
        for (Account account : accountList) {
            counter += getTransactionsToAppliedWithAccountId(account);
        }
        return counter;
    }

    @Override
    public int getTransactionsToAppliedWithAccountId(Account account) {

        Query query = em.createNamedQuery(Transaction.getUnappliedTransactions, Integer.class);
        query.setParameter("accountIdFrom", account);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public TransactionResponse insertTransaction(TransactionRequest transactionRequest, User user) {

        TransactionResponse transactionResponse = new TransactionResponse();
        transactionResponse.setResult(false);
        try {
            utx.begin();

            Account accountSource = this.getAccount(transactionRequest.getSource());
            Account accountDestination = this.getAccount(transactionRequest.getDestination());

            if (accountSource.getBalance().subtract(transactionRequest.getAmount()).compareTo(new BigDecimal(0)) < 0) {
                transactionResponse.setMessage("Vous ne disposez pas d'un solde suffisant.");
                return transactionResponse;
            }

            transactionResponse.setResult(true);

            Transaction transaction = new Transaction();

            transaction.setAccountIdFrom(accountSource);
            transaction.setAccountIdTo(accountDestination);
            transaction.setAmount(transactionRequest.getAmount());

            if (user.getType().equals("advisor") || transactionRequest.getAmount().compareTo(new BigDecimal(1000)) < 0) {
                transaction.setApplied(0);
                transactionResponse.setMessage("Le transaction est effective.");

                accountSource.setBalance(accountSource.getBalance().subtract(transactionRequest.getAmount()));
                accountDestination.setBalance(accountDestination.getBalance().add(transactionRequest.getAmount()));

            } else {
                transaction.setApplied(1);
                transactionResponse.setMessage("La transaction sera validée dès que possible par votre conseiller");
            }
            transaction.setAuthor(user);
            transaction.setComment(transactionRequest.getComment());
            transaction.setDate(Date.valueOf(LocalDate.now()));

            em.persist(transaction);
            em.flush();
            utx.commit();
        } catch (NotSupportedException | SystemException | HeuristicMixedException | HeuristicRollbackException | RollbackException e) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException ex) {
                ex.printStackTrace();
            }
        }

        return transactionResponse;
    }

}
