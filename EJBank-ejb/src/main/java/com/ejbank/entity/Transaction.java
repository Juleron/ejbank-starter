package com.ejbank.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@NamedQueries({
        @NamedQuery(name = Transaction.getUnappliedTransactions, query = "select count(t) from Transaction t where t.accountIdFrom = :accountIdFrom and t.applied = 1"),
        @NamedQuery(name = Transaction.getTransactions, query = "select t from Transaction t where " +
                "t.accountIdFrom = :accountId or t.accountIdTo = :accountId"),
        @NamedQuery(name = Transaction.getNumberTransactions, query = "select count(t) from Transaction t where " +
                "t.accountIdFrom = :accountId or t.accountIdTo = :accountId")
})
@Entity
@Table(name = "ejbank_transaction", schema = "ejbank")
public class Transaction implements Serializable {

    private static final long serialVersionUID = -7200188998808680110L;

    public static final String getUnappliedTransactions = "getUnappliedTransactions";
    public static final String getTransactions = "getTransactions";
    public static final String getNumberTransactions = "getNumberTransactions";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "account_id_from", referencedColumnName = "id")
    private Account accountIdFrom;

    @ManyToOne
    @JoinColumn(name = "account_id_to", referencedColumnName = "id")
    private Account accountIdTo;

    @OneToOne
    @JoinColumn(name = "author", referencedColumnName = "id")
    private User author;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "comment")
    private String comment;

    @Column(name = "date")
    private Date date;

    @Column(name = "applied")
    private int applied;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccountIdFrom() {
        return accountIdFrom;
    }

    public void setAccountIdFrom(Account accountIdFrom) {
        this.accountIdFrom = accountIdFrom;
    }

    public Account getAccountIdTo() {
        return accountIdTo;
    }

    public void setAccountIdTo(Account accountIdTo) {
        this.accountIdTo = accountIdTo;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getApplied() {
        return applied;
    }

    public void setApplied(int applied) {
        this.applied = applied;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", accountIdFrom=" + accountIdFrom +
                ", accountIdTo=" + accountIdTo +
                ", author=" + author +
                ", amount=" + amount +
                ", comment='" + comment + '\'' +
                ", date=" + date +
                ", applied=" + applied +
                '}';
    }

}
