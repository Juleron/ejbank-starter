package com.ejbank.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ejbank_advisor", schema = "ejbank")
@DiscriminatorValue(value = "advisor")
public class Advisor extends User implements Serializable {

    private static final long serialVersionUID = 2452362122911583074L;

    @OneToMany
    @JoinColumn(name = "advisor_id", referencedColumnName = "id")
    private List<Customer> customers;

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Account> getAccounts() {
        List<Account> accounts = new ArrayList<>();
        for (Customer customer : getCustomers()) {
            accounts.addAll(customer.getAccounts());
        }
        return accounts;
    }
}
