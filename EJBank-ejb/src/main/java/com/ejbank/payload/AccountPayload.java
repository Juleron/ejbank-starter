package com.ejbank.payload;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountPayload implements Serializable {
    private int id;
    private String type;
    private BigDecimal amount;
    private String user;
    private int validation;

    public AccountPayload() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getValidation() {
        return validation;
    }

    public void setValidation(int validation) {
        this.validation = validation;
    }
}
