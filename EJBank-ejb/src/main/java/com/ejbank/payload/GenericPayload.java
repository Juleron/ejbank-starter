package com.ejbank.payload;

import java.io.Serializable;

public class GenericPayload implements Serializable {
    private String error;

    public GenericPayload() {
    }

    public GenericPayload(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
