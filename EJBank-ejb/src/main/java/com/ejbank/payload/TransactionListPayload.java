package com.ejbank.payload;

import java.util.List;

public class TransactionListPayload extends GenericPayload {
    private int total;
    private List<ITransactionPayload> transactions;

    public TransactionListPayload() {

    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ITransactionPayload> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<ITransactionPayload> transactions) {
        this.transactions = transactions;
    }
}
