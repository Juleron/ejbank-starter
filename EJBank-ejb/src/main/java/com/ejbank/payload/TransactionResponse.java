package com.ejbank.payload;

public class TransactionResponse {
    private boolean result;
    private String message;

    public TransactionResponse() {

    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
