package com.ejbank.payload;

import java.util.List;

public class AccountsPayload extends GenericPayload {
    private List<AccountPayload> accounts;

    public AccountsPayload(List<AccountPayload> accountPayloads) {
        this.accounts = accountPayloads;
    }

    public List<AccountPayload> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountPayload> accounts) {
        this.accounts = accounts;
    }

}
