package com.ejbank.payload;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ejbank.entity.Account;
import com.ejbank.entity.User;

public class AccountDetailsPayload extends GenericPayload implements Serializable {
	private static final long serialVersionUID = 1L;

	private String owner;
	private String advisor;
	private BigDecimal rate;
	private BigDecimal interest;
	private BigDecimal amount;
	
	public AccountDetailsPayload(Account a, User owner, BigDecimal interest) {
		this.owner = a.getUser().getFirstname()+" "+a.getUser().getLastname();
		this.advisor = owner.getFirstname()+" "+owner.getLastname();
		this.rate = a.getAccountType().getRate();
		this.interest = interest;
		this.amount = a.getBalance();
	}

	public String getOwner() {
		return owner;
	}

	public String getAdvisor() {
		return advisor;
	}
	
	public BigDecimal getRate() {
		return rate;
	}
	
	public BigDecimal getInterest() {
		return interest;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

}
