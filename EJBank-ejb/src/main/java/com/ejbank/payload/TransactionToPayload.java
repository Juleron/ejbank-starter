package com.ejbank.payload;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class TransactionToPayload extends GenericPayload implements Serializable, ITransactionPayload {
    private int id;
    private Date date;
    private String source;
    private String source_user;
    private String destination;
    private BigDecimal amount;
    private String author;
    private TransactionState state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public TransactionState getState() {
        return state;
    }

    public void setState(TransactionState state) {
        this.state = state;
    }

    public String getSource_user() {
        return source_user;
    }

    public void setSource_user(String source_user) {
        this.source_user = source_user;
    }
}
