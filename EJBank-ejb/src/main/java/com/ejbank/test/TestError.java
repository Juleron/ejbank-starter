package com.ejbank.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ejbank.error.BeanException;
import com.ejbank.error.MessageCode;

@SuppressWarnings("static-method")
public class TestError {

	@Test
	public void shouldShowClientMessageCorrectly() {
		for (MessageCode messageCode: MessageCode.values()) {
			BeanException test = BeanException.createException(messageCode);
			assertEquals(test.getClientMessage(), messageCode.getMessage());
		}
	}
	
	@Test
	public void shouldKeepCauseWhenSpecified() {
		BeanException test = BeanException.createException(MessageCode.values()[0], new IllegalArgumentException("test"));
		assertEquals(test.getMessage(), MessageCode.values()[0].getMessage()+"\nCaused by :\ntest");
	}
	
}
