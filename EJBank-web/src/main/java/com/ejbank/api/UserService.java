package com.ejbank.api;

import com.ejbank.entity.User;
import com.ejbank.payload.UserPayload;
import com.ejbank.beans.IUserBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class UserService {

    @EJB
    private IUserBean userBean;

    @GET
    @Path("/{user_id}")
    public UserPayload getUserEJB(@PathParam("user_id") Integer userId) {
        Objects.requireNonNull(userId);
        User user = userBean.getUser(userId);
        return new UserPayload(user.getFirstname(), user.getLastname());
    }

    @GET
    @Path("/all")
    public List<User> getAll() {
        return userBean.getUsers();
    }
}
