package com.ejbank.api;

import com.ejbank.beans.IAccountBean;
import com.ejbank.beans.IUserBean;
import com.ejbank.entity.Account;
import com.ejbank.entity.User;
import com.ejbank.payload.AccountDetailsPayload;
import com.ejbank.payload.GenericPayload;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AccountService {

    @EJB
    private IAccountBean accountBean;

    @EJB
    private IUserBean userBean;

    @GET
    @Path("/{account_id}/{user_id}")
    public GenericPayload getAccountsAttached(@PathParam("account_id") Integer accountId, @PathParam("user_id") Integer userId) {
        Objects.requireNonNull(accountId);
        Objects.requireNonNull(userId);

        User user = userBean.getUser(userId);
        if (user == null) {
            return new GenericPayload("Unable to get the user identified by " + userId + ". The user doesn't exists");
        }

        List<Account> accounts = user.getAccounts();
        Optional<Account> selectedAccount = Optional.empty();

        for (Account a : accounts) {
            if (a.getId() == accountId) {
                selectedAccount = Optional.of(a);
            }
        }

        if (selectedAccount.isPresent()) {
            return new AccountDetailsPayload(selectedAccount.get(), user, accountBean.getInterests(selectedAccount.get()));
        }

        return new GenericPayload("Unable to get the account identified by "+accountId+". The account doesn't exists");
    }

}