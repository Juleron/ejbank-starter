package com.ejbank.api;

import com.ejbank.beans.IAccountBean;
import com.ejbank.beans.IUserBean;
import com.ejbank.entity.Account;
import com.ejbank.entity.User;
import com.ejbank.payload.AccountDetailsPayload;
import com.ejbank.payload.AccountPayload;
import com.ejbank.payload.AccountsPayload;
import com.ejbank.payload.GenericPayload;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AccountsService {

    @EJB
    private IAccountBean accountBean;

    @EJB
    private IUserBean userBean;

    @GET
    @Path("/{user_id}")
    public AccountsPayload getUserEJB(@PathParam("user_id") Integer userId) {
        Objects.requireNonNull(userId);
        List<Account> accounts = accountBean.getAccounts(userId);
        List<AccountPayload> accountPayloads = new ArrayList<>();
        for (Account account : accounts) {
            AccountPayload accountPayload = new AccountPayload();
            accountPayload.setId(account.getId());
            accountPayload.setType(account.getAccountType().getName());
            accountPayload.setAmount(account.getBalance());
            accountPayloads.add(accountPayload);
        }

        return new AccountsPayload(accountPayloads);
    }

    @GET
    @Path("/attached/{user_id}")
    public GenericPayload getAccountsAttached(@PathParam("user_id") Integer userId) {
        Objects.requireNonNull(userId);

        User user = userBean.getUser(userId);
        List<Account> accounts = new ArrayList<>();
        switch (user.getType()) {
            case "customer":
                return new GenericPayload("Vous ne pouvez pas accéder à cette page.");
            case "advisor":
                accounts = accountBean.getAccounts(userId);
                break;
            default:
                System.out.println("Unknown type user.");
        }

        List<AccountPayload> accountPayloads = new ArrayList<>();
        for (Account account : accounts) {
            AccountPayload accountPayload = new AccountPayload();
            accountPayload.setId(account.getId());
            accountPayload.setType(account.getAccountType().getName());
            accountPayload.setAmount(account.getBalance());
            accountPayload.setUser(account.getUser().getFirstname() + " " + account.getUser().getLastname());
            accountPayload.setValidation(accountBean.getTransactionsToAppliedWithAccountId(account));
            accountPayloads.add(accountPayload);
        }

        return new AccountsPayload(accountPayloads);
    }

    @GET
    @Path("/account/{account_id}/{user_id}")
    public GenericPayload getAccountsAttached(@PathParam("account_id") Integer accountId, @PathParam("user_id") Integer userId) {
    	Objects.requireNonNull(accountId);
    	Objects.requireNonNull(userId);

    	User user = userBean.getUser(userId);
    	if (user == null) {
    		return new GenericPayload("Unable to get the user identified by "+userId+". The user doesn't exists");
    	}

    	List<Account> accounts = user.getAccounts();
    	Optional<Account> selectedAccount = Optional.empty();

    	for (Account a : accounts) {
    		if (a.getId() == accountId) {
    			selectedAccount = Optional.of(a);
    		}
    	}

    	if (selectedAccount.isPresent()) {
    		return new AccountDetailsPayload(selectedAccount.get(), user, accountBean.getInterests(selectedAccount.get()));
    	}
    	return new GenericPayload("Unable to get the account identified by "+accountId+". The account doesn't exists");
    }

    @GET
    @Path("/all/{user_id}")
    public AccountsPayload getAllAccounts(@PathParam("user_id") Integer userId) {
        Objects.requireNonNull(userId);

        User user = userBean.getUser(userId);
        List<Account> accounts = accountBean.getAccounts(userId);

        List<AccountPayload> accountPayloads = new ArrayList<>();
        for (Account account : accounts) {
            AccountPayload accountPayload = new AccountPayload();
            accountPayload.setId(account.getId());
            accountPayload.setType(account.getAccountType().getName());
            accountPayload.setAmount(account.getBalance());
            accountPayload.setUser(account.getUser().getFirstname() + " " + account.getUser().getLastname());
            accountPayloads.add(accountPayload);
        }

        return new AccountsPayload(accountPayloads);
    }
}