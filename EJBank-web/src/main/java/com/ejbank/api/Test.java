package com.ejbank.api;

import com.ejbank.entity.User;
import com.ejbank.payload.UserPayload;
import com.ejbank.beans.IUserBean;
import com.ejbank.test.TestBeanLocal;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class Test {

    @EJB
    private TestBeanLocal testBean;

    @EJB
    private IUserBean userBean;
    
    @GET
    @Path("/ejb")
    public String testEJB() {
        return testBean.test();
    }

    @GET
    @Path("/users")
    public List<UserPayload> getUserEJB() {
        List<UserPayload> userPayloads = new ArrayList<>();
        for (User user : userBean.getUsers()) {
            userPayloads.add(new UserPayload(user.getFirstname(), user.getLastname()));
        }
        return userPayloads;
    }
}
