package com.ejbank.api;

import com.ejbank.beans.IAccountBean;
import com.ejbank.beans.IUserBean;
import com.ejbank.entity.Account;
import com.ejbank.entity.User;
import com.ejbank.payload.GenericPayload;
import com.ejbank.payload.TransactionPreview;
import com.ejbank.payload.TransactionRequest;
import com.ejbank.payload.TransactionResponse;
import com.ejbank.payload.TransactionValidation;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.Objects;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class TransactionService {

    @EJB
    private IAccountBean accountBean;

    @EJB
    private IUserBean userBean;

    @GET
    @Path("/validation/notification/{user_id}")
    public int notifValidation(@PathParam("user_id") Integer userId) {
        Objects.requireNonNull(userId);

        if (!userBean.getUser(userId).getType().equals("advisor")) {
            return 0;
        }

        return accountBean.getTransactionsToAppliedWithUserId(userId);
    }

    @POST
    @Path("/validation")
    public GenericPayload transactionValidation(TransactionValidation transactionValidation) {
        Objects.requireNonNull(transactionValidation);

        User user = userBean.getUser(transactionValidation.getAuthor());

        if (!user.getType().equals("advisor")) {
            return new GenericPayload("Vous ne pouvez pas valider cette opération.");
        }

        return accountBean.updateTransactionValidation(transactionValidation);
    }

    @POST
    @Path("/preview")
    public TransactionPreview previewTransaction(TransactionRequest transactionRequest) {
        Objects.requireNonNull(transactionRequest);

        System.out.println("Source : " + transactionRequest.getSource());
        if (transactionRequest.getSource() == 0) {
            TransactionPreview tmpPreview = new TransactionPreview();
            tmpPreview.setError("Aie aie aie");
            return tmpPreview;
        }

        Account accountSource = accountBean.getAccount(transactionRequest.getSource());
        Account accountDestination = accountBean.getAccount(transactionRequest.getDestination());

        TransactionPreview transactionPreview = new TransactionPreview();

        BigDecimal result = accountSource.getBalance().subtract(transactionRequest.getAmount());

        transactionPreview.setBefore(result);
        transactionPreview.setAfter(accountDestination.getBalance().add(transactionRequest.getAmount()));

        if (result.compareTo(new BigDecimal(0)) < 0) {
            transactionPreview.setResult(false);
            transactionPreview.setError("Vous ne disposez pas de fonds suffisants pour réaliser cette transaction.");
            return transactionPreview;
        }

        transactionPreview.setResult(true);
        transactionPreview.setMessage("Vous pouvez réaliser cette transaction.");
        return transactionPreview;
    }

    @POST
    @Path("/")
    public TransactionResponse transaction(TransactionRequest transactionRequest) {

        User user = userBean.getUser(transactionRequest.getAuthor());

        return accountBean.insertTransaction(transactionRequest, user);
    }

    @GET
    @Path("/list/{account_id}/{offset}/{user_id}")
    public GenericPayload transactionList(@PathParam("account_id") Integer accountId,
                                          @PathParam("offset") Integer offset,
                                          @PathParam("user_id") Integer userId) {
        Objects.requireNonNull(accountId);
        Objects.requireNonNull(offset);
        Objects.requireNonNull(userId);

        return accountBean.getTransactions(accountId, offset, userId);
    }
}